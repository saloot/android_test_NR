//
//  WebService.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import Foundation
import Alamofire

class WebService {
    // -------------------------- const
    internal static let Achareh_API = "http://dev.achareh.ir/api"
    internal static let Achareh_address = Achareh_API + "/karfarmas/address"
    
    
    static func sendUserInformation(user : UserInfo , viewController : UIViewController , completion: @escaping (_ success:Bool , _ dic:NSDictionary) -> ()) {
        let username = "karfarma_test"
        let password =  "12345678"
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
        let params = [ "region": 1 , "address" : user.address! , "lat" : user.lat! , "lng" : user.lat! , "coordinate_mobile" : user.phoneNumber! , "coordinate_phone_number" : user.telephoneNumber! , "first_name" : user.lName! , "last_name" : user.fName! , "gender" : user.gender!] as [String : Any]
        let ulr =  URL(string: Achareh_address)!
        var request = URLRequest(url: ulr as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let data = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        request.addValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
        print("address == \(params)")
        
        
        //Check Internet Connection
        if Connectivity.isConnectedToInternet {
            Alamofire.request(request as URLRequestConvertible)
                .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("❌ Error while fetching Address : \(String(describing: response.result.error))")
                        completion(false , [:])
                        return
                    }
                    
                    guard let value = response.result.value as? NSDictionary else {
                        print("")
                        Tools.showError(errorTitle: .error , errorMessage: .serverError , viewController: viewController)
                        completion(false , [:])
                        return
                    }
                    if response.response?.statusCode == 201 || response.response?.statusCode == 200 {
                        completion(true , value)
                    }
                    else if response.response?.statusCode == 401 {
                        Tools.showError(errorTitle: .error , errorMessage: .authenticationError , viewController: viewController)
                        completion(false , [:])
                    }
                    else if response.response?.statusCode == 500 {
                        Tools.showError(errorTitle: .error , errorMessage: .serverError , viewController: viewController)
                        completion(false , [:])
                    }
                    
            }
        }
        else{
            Tools.showError(errorTitle: .notice , errorMessage: .networkError, viewController: viewController)
        }
    }
    static func getInformation(viewController : UIViewController , completion: @escaping (_ success:Bool , _ array:[NSDictionary]) -> ()){
        let username = "karfarma_test"
        let password =  "12345678"
        let credentialData = "\(username):\(password)".data(using: String.Encoding.utf8)!
        let base64Credentials = credentialData.base64EncodedString(options: [])
        
       
        let ulr =  URL(string: Achareh_address)!
        var request = URLRequest(url: ulr as URL)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic \(base64Credentials)", forHTTPHeaderField: "Authorization")
        
        //Check Internet Connection
        if Connectivity.isConnectedToInternet {
            
            Alamofire.request(request as URLRequestConvertible)
                .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("❌ Error while fetching Address : \(String(describing: response.result.error))")
                        Tools.showError(errorTitle: .error , errorMessage: .serverError , viewController: viewController)
                        completion(false , [])
                        return
                    }
                    
                    guard let value = response.result.value as? [NSDictionary] else {
                        print("")
                        Tools.showError(errorTitle: .error , errorMessage: .serverError , viewController: viewController)
                        completion(false , [])
                        return
                    }
                    if response.response?.statusCode == 200 {
                        completion(true , value)
                    }
                    else if response.response?.statusCode == 401 {
                        Tools.showError(errorTitle: .error , errorMessage: .authenticationError , viewController: viewController)
                        completion(false , [])
                    }
                    else if response.response?.statusCode == 500 {
                        Tools.showError(errorTitle: .error , errorMessage: .serverError , viewController: viewController)
                        completion(false , [])
                    }
                    
            }
        }
        else{
           Tools.showError(errorTitle: .notice , errorMessage: .networkError, viewController: viewController)
        }
        
    }
    
}
