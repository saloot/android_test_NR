//
//  UserInfo.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import Foundation

class UserInfo {
    
    static let sharedInstance = UserInfo()
    
    var fName : String?
    var lName : String?
    var phoneNumber : String?
    var telephoneNumber : String?
    var address : String?
    var lat : Double?
    var lng : Double?
    var gender : String?
    
    private init(){}
    
    static func isCompletedInformation(user : UserInfo) -> Bool {
        if user.fName != "" && user.fName != nil && user.lName != "" && user.lName != nil && user.phoneNumber != "" && user.phoneNumber != nil && user.telephoneNumber != "" && user.telephoneNumber != nil && user.address != "" && user.address != nil && user.gender != "" && user.gender != nil {
            return true
        }
        else {
            return false
        }
    }
   
}
