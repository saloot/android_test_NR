//
//  TextFieldCellTableViewCell.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    var valiadation : Bool = false
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellView.layer.cornerRadius = 2
        self.cellView.layer.borderColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0).cgColor
        self.cellView.layer.borderWidth = 0.5
        
        self.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 2 || textField.tag == 3 {
            textField.text = Tools.convertToPersian(text: textField.text!)
        }
        
    }

}
