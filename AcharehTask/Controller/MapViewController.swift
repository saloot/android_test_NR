//
//  MapViewController.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController , CLLocationManagerDelegate {
    
    //IBOutlet
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var mapView: GMSMapView!
    
    //Var & Let
    var loading:DialogLoading!
    let marker =  GMSMarker()
    let locationMarker = GMSMarker()
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        loading = DialogLoading(view: self.view)
        
        if UIDevice.current.iPhoneX {
            topContraint.constant = 45
            heightConstraint.constant = 80
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        
        mapView.delegate = self
        //mapView.settings.myLocationButton = true
        let camera = GMSCameraPosition.camera(withLatitude: 37.36, longitude: -122.0, zoom: 6.0)
        mapView.camera = camera
        showMarker(position: camera.target)
        
    }
    
    
    func showMarker(position: CLLocationCoordinate2D){
        marker.position = position
        marker.map = mapView
    }

    // MARK: CLLocation Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 15);
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
        
        marker.map = self.mapView
        locationManager.stopUpdatingLocation()
        
    }

    @IBAction func acceptLocationBtn(_ sender: Any) {
        
      
        UserInfo.sharedInstance.lat = marker.position.latitude
        UserInfo.sharedInstance.lng = marker.position.longitude
        
        WebService.sendUserInformation(user: UserInfo.sharedInstance , viewController: self) { (success, dic) in
            self.loading.start()
            if success {
               self.loading.stop()
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "ResponseViewController") as! ResponseViewController
               self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                self.loading.stop()
            }
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension MapViewController: GMSMapViewDelegate {
    //MARK - GMSMarker Dragging
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return true
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapView.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        marker.position = position.target
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        self.mapView.isMyLocationEnabled = true
        if (gesture) {
            mapView.selectedMarker = nil
        }
    }
    
}


