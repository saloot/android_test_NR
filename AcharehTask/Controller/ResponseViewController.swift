//
//  ResponseViewController.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {

    //IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //Var & Let
    var loading:DialogLoading!
    var responseArray : [NSDictionary] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        tableView.delegate = self
        tableView.dataSource = self
        loading = DialogLoading(view: self.view)
        loading.start()
        tableView.estimatedRowHeight = 140
         tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.getUserInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
  
}

//MARK: TableView Delegate
extension ResponseViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseArray.count == 0 {
            return 0
        }
        else{
            return responseArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        let dic = self.responseArray[indexPath.row]
        cell.addressLabel.text = dic.value(forKey: "address") as? String
        cell.nameLabel.text = dic.value(forKey: "first_name") as? String
        cell.phoneNumberLabel.text = Tools.convertToPersian(text: dic.value(forKey: "coordinate_mobile") as! String)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
 
}


//MARK: API Call
extension ResponseViewController {
    func getUserInfo() {
        WebService.getInformation(viewController: self) { (success, array) in
            self.loading.stop()
            if success {
                self.responseArray = array
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        }
    }
}
