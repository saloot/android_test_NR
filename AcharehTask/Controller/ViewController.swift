//
//  ViewController.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var addressTextFieldView: UIView!
    @IBOutlet weak var addressImageValidation: UIImageView!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var addressValidation : Bool = false
    let user = UserInfo.sharedInstance

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        if UIDevice.current.iPhoneX {
            topContraint.constant = 45
            heightConstraint.constant = 80
        }
        
        self.addressTextFieldView.layer.cornerRadius = 2
        self.addressTextFieldView.layer.borderColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0).cgColor
        self.addressTextFieldView.layer.borderWidth = 0.5
        
        self.addressTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.addressTextField.delegate = self
        
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .none
        
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        
        
        user.gender = "Male"
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tableHeightConstraint.constant = tableView.contentSize.height
        
        self.addressTextField.text = user.address ?? ""
        
    }
    
    
    @IBAction func nextStepBtn(_ sender: Any) {
        if UserInfo.isCompletedInformation(user: user) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            Tools.showError(errorTitle: .notice, errorMessage: .notFilled, viewController: self)
        }
    }
    
    @IBAction func genderSegmentAction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            print("خانم")
            user.gender = "Female"
            
        case 1:
            print("آقا")
            user.gender = "Male"
        default:
            break
        }
    }
    
}



extension ViewController {
    //MARK: TextField Tools
    @objc func textFieldDidChange(_ textField: UITextField) {
        textField.text = Tools.convertToPersian(text: textField.text!)
    }
    
    //MARK: ScrollView with Keyboard
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}

extension ViewController : UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate {
    
    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let TextFieldCell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath) as! TextFieldCell
        
        
        TextFieldCell.textField.delegate = self
        TextFieldCell.textField.tag = indexPath.row
        
        switch indexPath.row {
        case 0:
           TextFieldCell.titleLabel.text = "نام"
           TextFieldCell.textField.text = user.fName ?? ""
        case 1:
            TextFieldCell.titleLabel.text = "نام خانوادگی"
            TextFieldCell.textField.text = user.lName ?? ""
        case 2:
            TextFieldCell.titleLabel.text = "تلفن همراه"
            TextFieldCell.textField.text = user.phoneNumber ?? ""
        case 3:
            TextFieldCell.titleLabel.text = "تلفن ثابت"
            TextFieldCell.textField.text = user.telephoneNumber ?? ""
        default:
            print("")
        }
        
        return TextFieldCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 70
    
    }
    
    
    //MARK: TextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let cell = tableView.cellForRow(at: IndexPath(row: textField.tag , section: 0)) as? TextFieldCell else { return }
        guard let text = textField.text else { return }
        print(text)
        if textField.tag == 2 || textField.tag == 3 {
            textField.keyboardType = .phonePad
        }
        cell.cellView.layer.borderColor = UIColor.black.cgColor
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        
        if textField == addressTextField {
             addressTextFieldView.layer.borderColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0).cgColor
            if addressValidation {
                user.address = textField.text
            }
            else{
                user.address = ""
            }
            
        }
        else {
            let cell = tableView.cellForRow(at: IndexPath(row: textField.tag , section: 0)) as! TextFieldCell
            cell.cellView.layer.borderColor = UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0).cgColor
            if cell.valiadation{
                switch textField.tag {
                case 0:
                    user.fName = textField.text
                case 1:
                    user.lName = textField.text
                case 2:
                    user.phoneNumber = Tools.convertToEnglish(text: textField.text ?? "")
                case 3:
                    user.telephoneNumber = Tools.convertToEnglish(text: textField.text ?? "")
                default:
                    print("")
                }
            }
            else{
                switch textField.tag {
                case 0:
                    user.fName = ""
                case 1:
                    user.lName = ""
                case 2:
                    user.phoneNumber = ""
                case 3:
                    user.telephoneNumber = ""
                default:
                    print("")
                }

            }
           
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        
        if textField != addressTextField {
          let cell = tableView.cellForRow(at: IndexPath(row: textField.tag , section: 0)) as! TextFieldCell
            if textField.tag == 2 || textField.tag == 3 {
                if count >= 11 {
                    cell.cellImage.image = UIImage(named: "checked-symbol")
                    cell.valiadation = true
                    
                }
                else{
                    cell.cellImage.image = UIImage(named: "filled-circle-2")
                    cell.valiadation = false
                    
                }
                    return count <= 11
            }
            
            else {
                if count >= 3 {
                    cell.cellImage.image = UIImage(named: "checked-symbol")
                    cell.valiadation = true
                }
                else{
                    cell.cellImage.image = UIImage(named: "filled-circle-2")
                    cell.valiadation = false
                }
                return true
            }
        }
            else {
                if count >= 11 {
                    self.addressImageValidation.image = UIImage(named: "checked-symbol")
                    self.addressValidation = true
                }
                else{
                    addressImageValidation.image = UIImage(named: "filled-circle-2")
                    self.addressValidation = false
                }
                return true
            }
        }
}





    



