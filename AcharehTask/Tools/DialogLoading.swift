
import UIKit

class DialogLoading {
    
    var indicator:UIActivityIndicatorView!
    var parent:UIView!
    var view:UIView
    
    internal func start() {
        if parent == nil {
            createView()
        }
        view.addSubview(parent)
    }
    
    func stop() {
        if parent != nil {
            parent.removeFromSuperview()
        }
    }
    
    init(view:UIView){
        self.view = view
    }
    
    fileprivate func createView(){
        let rect =  CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: view.frame.height)
        parent = UIView(frame: rect)
        parent.backgroundColor = UIColor.white
        parent.alpha = 0.95
        indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = view.center;
        indicator.alpha = 1.0
        indicator.color = UIColor.black
        parent.addSubview(indicator)
        indicator.startAnimating()
        OverrideFonts.setFont(parent)
    }
    
}
