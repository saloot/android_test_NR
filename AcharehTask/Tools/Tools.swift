//
//  Tools.swift
//  AcharehTask
//
//  Created by Rojin Rn on 11/13/18.
//  Copyright © 2018 Rojin Rn. All rights reserved.
//

import Foundation
import UIKit

class Tools {
    
    
    enum errorMessage : String {
        case networkError = "لطفا ارتباط خود به اینترنت را بررسی کنید."
        case serverError = "خطا در ارتباط با سرور."
        case notFilled = "لطفا فیلد های موجود را به درستی وارد کنید."
        case authenticationError = "شما هنوز ثبت نام نکرده اید."
    }
    enum errorTitle : String {
        case notice = "توجه"
        case error = "خطا"
        case success = "تبریک"
       
    }
    
    internal class func phoneLength(_ phone:String) -> Bool {
        return phone.count == 11
    }
    internal class func nameLength(_ name:String) -> Bool {
        return name.count >= 3
    }
    internal class func convertToPersian(text : String)-> String {
        let numbersDictionary : Dictionary = ["0" : "۰","1" : "۱", "2" : "۲", "3" : "۳", "4" : "۴", "5" : "۵", "6" : "۶", "7" : "۷", "8" : "۸", "9" : "۹",",":","]
        var str : String = text
        
        for (key,value) in numbersDictionary {
            str =  str.replacingOccurrences(of: key, with: value)
        }
        
        return str
    }
    internal class func convertToEnglish(text : String)-> String {
        let numbersDictionary : Dictionary = ["۰" : "0","۱" : "1", "۲" : "2", "۳" : "3", "۴" : "4", "۵" : "5", "۶" : "6", "۷" : "7", "۸" : "8", "۹" : "9" , ",":","]
        var str : String = text
        
        for (key,value) in numbersDictionary {
            str =  str.replacingOccurrences(of: key, with: value)
        }
        
        return str
    }
    internal class func showError(errorTitle : errorTitle , errorMessage : errorMessage , viewController : UIViewController) {
        let alert = UIAlertController(title: errorTitle.rawValue , message: errorMessage.rawValue , preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "بستن", style: UIAlertAction.Style.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            
        }))
        
        viewController.present(alert, animated: true, completion: nil)
    }
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "تایید", style: .plain, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
